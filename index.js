const express = require('express');
const app = express();
const path = require('path');

const fs = require('fs');
const _ = require('lodash');
const engines = require('consolidate');

const users = [];
var userinfo = {};
function readFile(filename,calback){
    fs.readFile('users/'+filename, 'utf8', (err,data) => {
      if (err) throw err;
      userinfo = JSON.parse(data);
      calback();
    })

}
fs.readFile('users.json', {encoding: 'utf8'}, (err, data) => {
  if (err) throw err;

  _.forEach(JSON.parse(data), user => users.push(user));
});

/*
fs.readdir('users',(err,filenames) => {
  if (err) throw err;
  filenames.forEach((filename)=>{
    fs.readFile('users/'+filename, 'utf8', (err,data) =>{
      if (err) throw err;
      _.forEach(JSON.parse(data),userinfo => usersinfo.push(userinfo));
    })
  })
})
*/


app.engine('hbs', engines.handlebars);

app.set('views', './views');
app.set('view engine', 'hbs');

app.get('/', (req, res) => res.render('index', {users}));
app.get('/:username', (req, res) => {
    if (req.params.username != "favicon.ico") {
        readFile(req.params.username + ".json", ()=>{
            res.render('template',{userinfo});
        })
    }
});

const server = app.listen(3000, () => {
  console.log(`Server running at http://localhost:${server.address().port}`);
});
